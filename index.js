// === CONSIGNES ===

// Construisez un site qui permet de visualiser quelques photos du rover Curiosity. Pour cela, vous allez utiliser l'API suivante :

// https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY

// La page d'accueil "/" de votre site doit afficher la liste des images. Cette liste doit contenir l'ID de l'image, ainsi que la date de prise de vue (earth_date).

// Une seconde page - nommée "/image" - permet d'afficher les détails de la photo cliquée. Notez que cette page doit recevoir un paramètre, pour savoir quelle image doit être affichée en détails ! Cette page doit permettre de voir la photo, mais on doit aussi avoir le nom de la caméra qui a pris la photo, ainsi que le nom du rover.

const express = require("express");
const app = express();
const port = 3000;

app.set("view engine", "ejs");

const path = require("path");
app.use("/static", express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
    res.render("home", {});
});

app.get("/image", (req, res) => {
    res.render("image", {});
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
